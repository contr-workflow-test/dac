---
title: "New to HipChat: Connect dialogs"
date: "2015-06-23T16:00:00+07:00"
author: "pbrownlow"
categories: ["atlassian-connect", "hipchat"]
---
We are proud to announce that the HipChat UI is now extensible for add-on developers.
Powered by Atlassian Connect, you can now customise your HipChat experience by
surfacing arbitrary HTML in modal dialogs.

Dialogs can be opened from links sent in chat messages, which allows users to experience
rich, interactive, context-aware information without leaving the HipChat web client.
Great examples include opening calendar invite details or information on why your CI
test suite just failed. Dialogs are available to all HipChat add-ons, which can be kept
private for your team only, or distributed to thousands of others using the Atlassian
Marketplace. The availability of dialogs is just the first step in the road towards
extensibility within the HipChat client.

Raise your first HipChat dialog in a matter of minutes by following our [HipChat dialogs documentation](https://www.hipchat.com/docs/apiv2/dialogs?utm_source=dac&utm_medium=blog&utm_campaign=hipchat-connect-dialogs).

## What does a HipChat dialog look like?

Here are two examples of internally developed dialogs.

![Polly, a voting add-on](./images/polly.png)
![A maps add-on](./images/maps.png)

## I want it! How do I raise dialogs?
 
A super brief summary: post a link marked up with a `data-target="dialog"` attribute and with its `href` pointing to your web service, then in the HTML that you serve at that URL load a specified JS file. When users of the web client click the link they will see it as a dialog.

``` html
<a href="https://example.com/mydialog.html" data-target="dialog">Click me!</a>
```

Read more in our [HipChat dialogs documentation](https://www.hipchat.com/docs/apiv2/dialogs?utm_source=dac&utm_medium=blog&utm_campaign=hipchat-connect-dialogs).
If you've ever used [dialogs in JIRA or Confluence](https://developer.atlassian.com/static/connect/docs/latest/javascript/module-Dialog.html?utm_source=dac&utm_medium=blog&utm_campaign=hipchat-connect-dialogs) then you already know a lot about how they work in HipChat.
 
## What else is coming?
 
Right now, Connect dialogs are available only in the web client, and they will eventually make their way into other desktop clients. Users of clients that do not support dialogs will see your content in their web browsers, and when non-web clients begin supporting dialogs then users will simply see your dialogs inside these clients without you having to do any further work.

*[Build something amazing](https://www.hipchat.com/docs/apiv2/quick_start) with HipChat dialogs.*
