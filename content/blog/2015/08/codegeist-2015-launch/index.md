---
title: "Codegeist 2015: Enhance the tools you use"
date: "2015-08-26T16:00:00+07:00"
author: "dtester"
categories: ["hackathon"]
lede: "Do you have a great idea to make Atlassian JIRA, Confluence, HipChat, or Bitbucket even better? 
Join Atlassian's 8th add-on hackathon and win up to $100,000 in prizes. 
<a href='http://codegeist.atlassian.com'>Sign up now.</a>"
---
<p align="center">
    <a target="_blank" href="http://codegeist.atlassian.com">
        <img src="codegeist.png" alt="Codegeist Logo"/>
    </a>
</p>

We're delighted to announce Atlassian's 8th add-on hackathon. Do you have an idea to make 
JIRA, Confluence, HipChat, or Bitbucket even better? Build an add-on, launch it 
in the <a target="_blank" href="https://marketplace.atlassian.com/">
Atlassian Marketplace</a>, and you could be the next winner of Codegeist.

<p align="center">
    <a class="aui-button aui-button-primary" target="_blank" href="http://codegeist.atlassian.com"
        style="font-size: 20px; margin: 15px 0 15px 0;">
    Sign-up Now
    </a>
</p>

Whether you're an experienced add-on developer or trying something new, now is 
the time to build your add-on and share it with the community.

All participants will get $75 worth of AWS credits, free promotion of your add-on, 
and an exclusive Codegeist 2015 t-shirt. For the winners, we have prizes 
totaling more than $100,000!

Think you can build an awesome add-on? <a target="_blank" href="http://codegeist.atlassian.com/">
Sign up now</a> and list your add-on by October 19.

If you're new to developing add-ons, check out our quick start guides on the 
[Atlassian Developer][dac] site to start building today.

[dac]: https://developer.atlassian.com