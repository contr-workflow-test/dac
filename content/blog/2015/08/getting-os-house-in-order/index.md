---
title: "Getting Our Open Source House in Order"
date: "2015-08-28T16:00:00+07:00"
author: "mandyblack" 
categories: ["opensource"]
lede: "I'm a person who likes things to be tidy and organized. When things start to become unorganized and 
cluttered, it's time for spring cleaning. My latest spring cleaning project has been with Atlassian. We're 
taking a good look at our open source work and getting things organized."
---

I'm a person who likes things to be tidy and organized. In my ideal world, all the objects in my life 
have a purpose and a place where they live.  Then seemingly overnight it happens - it's 
been a while since I've done a good clean out, things are starting to become unorganized 
and cluttered - and it's driving me a bit crazy. Time for spring (or summer, winter, fall) cleaning!

![Spring Cleaning](feather-duster.jpg)
<p style="font-size: 0.8em;">[feather-duster.jpg] [3] licensed under [Creative Commons CC0] [4]</p>

Can you relate? Sometimes the same things can happen at work. My latest spring cleaning project has been with
Atlassian. We're taking a good look at our open source work. What do we currently have going on in the world of 
open source? Are things organized? Does everything we've put out still have a purpose and a home? One of our first steps 
in this process has been an audit of existing projects. Here are some of the things we discovered, along with 
our actions to get things in order.

## Atlassian supports OSS
We have a good amount of internal support around developing and contributing to Open Source Software. We don't 
want to simply consume open source; we want to help develop new projects and contribute to existing projects.
This is a part of the company culture that we want to continue to grow. 

## What are our internal processes?
If you are a developer at Atlassian, do you need permission to contribute to open source projects? If you are working 
on a project at Atlassian, do you know the process to open source the work? We have launched an internal Confluence 
space to help find the answers to these questions and more.   

## Where do we host our projects?
The vast majority of open source projects released by Atlassian are hosted on [Bitbucket] [1]. We plan to continue down 
this path. At the same time, we do not have a plan to migrate projects that are hosted elsewhere. Here are some stats.
* We have 180+ public project repos on Bitbucket with activity in 2015.
* Our presence on [GitHub] [2] is primarily a collection of repos we have forked from other projects. We do have 2-3 active projects hosted on GitHub.

## An archival strategy is needed 
Inactive open source projects hosted on the Atlassian Bitbucket or GitHub repositories should be archived over time. 
Archiving a project is not statement about the quality of work that went into the project. A project may be archived 
for any number of reasons.  For example, team resources to maintain the projects are no longer available, the project has 
become part of another project, or the project has simply reached the end of its life cycle.  

We are creating an archival strategy to relocate repositories that have little to no current activity. Archiving will 
not be a one time housekeeping activity.  This is something that we should be doing on a regular basis. Looking at 
the public repositories on Bitbucket, here are a few stats to illustrate this point:
* 71 repos with last activity in 2014 
* 68 repos with last activity in 2013 
* 41 repos with last activity in 2012 

## It's a team effort 
If your house is anything like mine, you can't organize it alone.  Without the buy in of all the family members, your 
efforts will be quickly defeated. That's the same with this spring cleaning project that we've been tackling at 
Atlassian. We've been asking for input from all Atlassians as we clarify our internal processes and develop strategies 
around archiving. It is exciting to see what our teams are already doing in the world of open source, and we look 
forward to putting tools and strategies in place to help us continue to move forward. 

If you have questions, recommendations, bright ideas, etc. for us, we welcome your thoughts here in the comments. In 
exchange for your feedback, we promise to keep everyone up to date on our progress.

[1]: https://bitbucket.org/atlassian/?utm_source=dac&utm_medium=blog&utm_campaign=getting-our-os-house-in-order	"Bitbucket"
[2]: https://github.com/atlassian/	"GitHub"
[3]: https://pixabay.com/en/feather-duster-cleaning-housework-709124/	"feather-duster.jpg"
[4]: https://creativecommons.org/publicdomain/zero/1.0/deed.en	"Creative Commons CC0"

