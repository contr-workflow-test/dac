## About REST API scopes

Scopes provide static authorization for add-ons. Scopes are defined in the add-on descriptor and specify the maximum set of actions that an add-on may perform: read, write, etc. This security level is enforced by Atlassian Connect and cannot be bypassed by add-on implementations.

#### How to read scope reference

The required scope for your add-on depends on how you interact with the resources that are exposed by the REST API. 

This page shows the required scope for GET, POST, PUT and DELETE operations on a resource path. REST resources are versioned by a path segment that varies with the version. The supported versions are listed under the path. This example shows how to read the table:

#### Example scope

| Path          | GET  | POST | PUT  | DELETE |
| ------------- | ---- | ---- | ---- | ------ |
|`/rest/atlassian-connect/{version}/license`<br>`1`, `latest`| `READ` | `N/A` | `N/A` | `N/A`|

A **GET** operation on `/rest/atlassian-connect/1/license` or `/rest/atlassian-connect/latest/license` requires your add-on to declare the **READ** scope. No other HTTP verbs are supported.

**Sub-resources:** Sub-resources are also available. For example, because `/rest/api/2/issue` is available, `/rest/api/2/issue/{key}/comment` is also available.

**Private APIs:** Some APIs are marked as `PRIVATE`. As opposed to public APIs, private APIs don't follow the principles described in the [Atlassian REST API Policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy). While we strive to keep private APIs backward-compatible, we cannot guarantee compatibility. Private APIs can also be replaced by new public APIs and may become deprecated over time.
