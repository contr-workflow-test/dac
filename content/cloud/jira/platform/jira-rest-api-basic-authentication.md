---
title: Basic auth for REST APIs
platform: cloud
product: jiracloud
category: devguide
subcategory: security
aliases:
- /jiracloud/jira-rest-api-basic-authentication-39991466.html
- /jiracloud/jira-rest-api-basic-authentication-39991466.md
confluence_id: 39991466
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39991466
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39991466
date: "2016-09-14"
---
# Basic auth for REST APIs

This page shows you how to allow REST clients to authenticate themselves using **[basic authentication](http://en.wikipedia.org/wiki/Basic_access_authentication)** (user name and password). This is one of three methods that you can use for authentication against the JIRA REST API; the other two being [cookie-based authentication] and [OAuth].

## Before you begin

Have you picked the right authentication method?

{{< include path="content/cloud/jira/platform/temp/authentication-methods.snippet.md">}}

<div class="aui-message note">
  <div class="icon"></div>
    <p class="title">
        <strong>Note</strong>
    </p>
    <p>
    This tutorial was last tested with <strong>JIRA 7.0</strong>.
    </p>
</div>



## Overview

JIRA's REST API is protected by the same restrictions which are provided via JIRAs standard web interface. This means that if you do not log in, you are accessing JIRA anonymously. Furthermore, if you log in and do not have permission to view something in JIRA, you will not be able to view it using the JIRA REST API either.

In most cases, the first step in using the JIRA REST API is to authenticate a user account with your JIRA site. Any authentication that works against JIRA will work against the REST API. On this page we will show you a simple example of basic authentication.

## Simple example

Most client software provides a simple mechanism for supplying a user name and password and will build the required authentication headers automatically. For example you can specify the -u argument with curl as follows

``` bash
curl -D- -u fred:fred -X GET -H "Content-Type: application/json" https://example.com:8081/rest/api/2/issue/createmeta
```

## Supplying basic auth headers

If you need to you may construct and send basic auth headers yourself. To do this you need to perform the following steps:

1.  Build a string of the form username:password
2.  Base64 encode the string
3.  Supply an "Authorization" header with content "Basic " followed by the encoded string. For example, the string "fred:fred" encodes to "ZnJlZDpmcmVk" in base64, so you would make the request as follows.

``` bash
curl -D- -X GET -H "Authorization: Basic ZnJlZDpmcmVk" -H "Content-Type: application/json" "https://example.com:8081/rest/api/2/issue/QA-31"
```

## Advanced topics

#### Authentication challenges

Because JIRA permits a default level of access to anonymous users, it does not supply a typical authentication challenge. Some HTTP client software expect to receive an authentication challenge before they will send an authorization header. This means that it may not behave as expected. In this case, you may need to configure it to supply the authorization header, as described above, rather than relying on its default mechanism.

#### CAPTCHA

CAPTCHA is 'triggered' after several consecutive failed log in attempts, after which the user is required to interpret a distorted picture of a word and type that word into a text field with each subsequent log in attempt. If CAPTCHA has been triggered, you cannot use JIRA's REST API to authenticate with the JIRA site.

You can check this in the error response from JIRA -- If there is an `X-Seraph-LoginReason` header with a a value of `AUTHENTICATION_DENIED`, this means the application rejected the login without even checking the password. This is the most common indication that JIRA's CAPTCHA feature has been triggered.

## Related pages

-   [JIRA REST API - Cookie-based Authentication][cookie-based authentication]
-   [JIRA REST API - OAuth authentication][OAuth]

  [Authentication for add-ons]: /cloud/jira/platform/addon-authentication
  [cookie-based authentication]: /cloud/jira/platform/jira-rest-api-cookie-based-authentication
  [OAuth]: /cloud/jira/platform/jira-rest-api-oauth-authentication
  [Connect Authentication documentation]: https://developer.atlassian.com/static/connect/docs/latest/concepts/authentication.html
