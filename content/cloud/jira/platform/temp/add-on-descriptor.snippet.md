# Add-on descriptor
The add-on descriptor is a JSON file (`atlassian-connect.json`) that describes the add-on to the Atlassian application. The descriptor includes general information for the add-on, as well as the modules that the add-on wants to use or extend.

If you're familiar with Java add-on development with previous versions of the Atlassian Plugin Framework, you may already be familiar with the `atlassian-plugin.xml` descriptors. The `atlassian-connect.json` serves the same function.

The descriptor serves as the glue between the remote add-on and the Atlassian application. When an administrator for a cloud instance installs an add-on, what they are really doing is installing this descriptor file, which contains pointers to your service. You can see an example below.

For details and application-specific reference information on the descriptor please refer to the "jira modules" and "confluence modules" sections of this documentation. But we'll call out a few highlights from the example here.

Since Atlassian Connect add-ons are remote and largely independent from the Atlassian application, they can be changed at any time, without having to create a new version or report the change to the Atlassian instance. The changes are reflected in the Atlassian instance immediately (or at least at page reload time).

However, some add-on changes require a change to the descriptor file itself. For example, an add-on could be modified to have a new page module. Since this requires a page module declaration in the descriptor, it means making an updated descriptor available. Until that updated descriptor is re-installed into the Atlassian Product that change in the descriptor file will not take effect. To propagate a change in the descriptor to the Atlassian products, you need to create a new version of the add-on in its Marketplace listing. The Marketplace will take care of the rest: informing administrators and automatically installing the available update. See [Upgrades](../developing/upgrades.html) for more details.