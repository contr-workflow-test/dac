---
title: Understanding JWT
platform: cloud
product: jiracloud
category: devguide
subcategory: security
aliases:
- /cloud/jira/platform/understanding-jwt.html
- /cloud/jira/platform/understanding-jwt.md
date: "2016-10-10"
---

{{< include path="content/cloud/connect/concepts/understanding-jwt.snippet.md" >}}
