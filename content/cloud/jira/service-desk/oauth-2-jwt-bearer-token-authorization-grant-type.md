---
title: "OAuth 2.0 - JWT Bearer token authorization grant type"
platform: cloud
product: jsdcloud
category: devguide
subcategory: security
date: "2016-10-31"
---
{{< include path="content/cloud/connect/concepts/OAuth2-JWT-Bearer-Token-Authentication.snippet.md">}}