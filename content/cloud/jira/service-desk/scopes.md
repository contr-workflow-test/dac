---
title: Scopes
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
aliases:
- /cloud/jira/service-desk/scopes.html
- /cloud/jira/service-desk/scopes.md
date: "2016-10-31"
---
{{< include path="content/cloud/connect/reference/jira-scopes.snippet.md">}}