---
title: About the JavaScript API
platform: cloud
product: jswcloud
category: reference
subcategory: jsapi
date: "2016-11-02"
---
{{< include path="content/cloud/connect/concepts/javascript-api.snippet.md">}}