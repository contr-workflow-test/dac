---
title: Context parameters
platform: cloud
product: jswcloud
category: devguide
subcategory: blocks
date: "2016-11-02"
---
{{< include path="content/cloud/connect/concepts/jira-context-parameters.snippet.md">}}
