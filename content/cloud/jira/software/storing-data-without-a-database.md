---
title: Storing data without a database
platform: cloud
product: jswcloud
category: devguide
subcategory: learning
date: 2016-12-13
aliases:
- /cloud/jira/software/storing-data-without-a-database.html
- /cloud/jira/software/storing-data-without-a-database.md
---
{{< include path="content/cloud/connect/concepts/storing-jira-data-without-a-database.md">}}