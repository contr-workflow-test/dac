---
title: Cqllistdatefields 39985908
aliases:
    - /confcloud/-cqllistdatefields-39985908.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985908
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985908
confluence_id: 39985908
platform:
product:
category:
subcategory:
---
# Confluence Connect : \_CQLListDateFields

-   [Created]
-   [Lastmodified]

  [Created]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-createdCreatedCreated
  [Lastmodified]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-lastmodifiedLastModifiedLastModified

