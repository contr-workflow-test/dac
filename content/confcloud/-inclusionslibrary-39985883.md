---
title: Inclusionslibrary 39985883
aliases:
    - /confcloud/-inclusionslibrary-39985883.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985883
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985883
confluence_id: 39985883
platform:
product:
category:
subcategory:
---
# Confluence Connect : \_InclusionsLibrary

The children of this page contain information which is **included in other pages**. This is a library of re-usable information chunks.

If you want to change any of these pages, be aware that:

-   Changing page names is problematic -- you will need to change all the {include} and {excerpt-include} macros manually.
-   The content is used in many places -- make sure your change is generic enough to fit the contexts in which the pages are used.

To include an excerpt from a page:

``` bash
{excerpt-include:_page name|nopanel=true}
```

Note that the page titled '\_page name' must contain the {excerpt} macro, otherwise the {excerpt-include} will not work.

To include the entire contents of a page"

``` bash
{include:page name|nopanel=true}
```

###### Children of this Page

-   [\_CQL]
    -   [\_CQLListDateFunctions]
    -   [\_CQLListUserFields]
    -   [\_CQLListUserFunctions]
    -   [\_CQLSupportedOpEquality]
    -   [\_CQLSupportedOpsRange]
    -   [\_CQLSupportedOpsSet]
    -   [\_CQLSupportedOpsText]
-   [\_CQLListDateFields]
-   [\_Note about REST APIs]
-   [\_REST URI]

  [\_CQL]: /confcloud/-cql-39985890.html
  [\_CQLListDateFunctions]: /confcloud/-cqllistdatefunctions-39985892.html
  [\_CQLListUserFields]: /confcloud/-cqllistuserfields-39985896.html
  [\_CQLListUserFunctions]: /confcloud/-cqllistuserfunctions-39985898.html
  [\_CQLSupportedOpEquality]: /confcloud/-cqlsupportedopequality-39985900.html
  [\_CQLSupportedOpsRange]: /confcloud/-cqlsupportedopsrange-39985902.html
  [\_CQLSupportedOpsSet]: /confcloud/-cqlsupportedopsset-39985904.html
  [\_CQLSupportedOpsText]: /confcloud/-cqlsupportedopstext-39985906.html
  [\_CQLListDateFields]: /confcloud/-cqllistdatefields-39985908.html
  [\_Note about REST APIs]: /confcloud/-note-about-rest-apis-39985886.html
  [\_REST URI]: /confcloud/-rest-uri-39985888.html

