---
title: Confluence REST API 39985291
aliases:
    - /confcloud/confluence-rest-api-39985291.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985291
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985291
confluence_id: 39985291
platform:
product:
category:
subcategory:
---
# Confluence Connect : Confluence REST API

The Confluence REST API is for admins who want to script interactions with Confluence and developers who want to integrate with or build on top of the Confluence platform.

For REST API documentation, go to <a href="https://docs.atlassian.com/atlassian-confluence/REST/latest/" class="external-link">Confluence REST API reference</a>.

## CRUD Operations

Confluence's REST APIs provide access to resources (data entities) via URI paths. To use a REST API, your application will make an HTTP request and parse the response. By default, the response format is JSON. Your methods will be the standard HTTP methods: GET, PUT, POST and DELETE. The REST API is based on open standards, so you can use any web development language to access the API.

For some examples of what you can do with the REST API, see [Confluence REST API Examples].

## Pagination

Confluence's REST API provides a way to paginate your calls to limit the amount of data you are fetching. Read more about this in [Pagination in the REST API]

## Expansions

Confluence's REST API also provides a way to fetch more data in a single call through REST API Expansions. Read more about this in [Expansions in the REST API].

## Advanced Search Through CQL

Confluence's REST API provides an [advanced search] that lets you use structured queries to search for content within Confluence. Your search results will take the same form as the Content model returned by the Content REST API. 

## Content and Space Properties

Content and Space Properties are JSON key-value storages that you can access through the REST API. This is a great way, for example, to store metadata about a piece (or pieces) of content. Read more about [Content Properties here.]

## REST API Policy

Read the [Atlassian REST API Policy ]for information on compatibility, versioning and deprecation. 

 

  [Confluence REST API Examples]: /confcloud/confluence-rest-api-examples-39985294.html
  [Pagination in the REST API]: /confcloud/pagination-in-the-rest-api-39985841.html
  [Expansions in the REST API]: /confcloud/expansions-in-the-rest-api-39985838.html
  [advanced search]: /confcloud/advanced-searching-using-cql-39985862.html
  [Content Properties here.]: /confcloud/content-properties-in-the-rest-api-39985912.html
  [Atlassian REST API Policy ]: https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy

