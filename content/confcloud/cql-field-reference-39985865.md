---
title: Cql Field Reference 39985865
aliases:
    - /confcloud/cql-field-reference-39985865.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985865
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985865
confluence_id: 39985865
platform:
product:
category:
subcategory:
---
# Confluence Connect : CQL Field Reference

A field in CQL is a word that represents an indexed property of content in Confluence. In a clause, a field is followed by an [operator], which in turn is followed by one or more values (or [functions]). The operator compares the value of the field with one or more values or functions on the right, such that only true results are retrieved by the clause.

**List of Fields:**

-   [Ancestor]
-   [Content]
-   [Created]
-   [Creator]
-   [Contributor]
-   [Favourite, favorite]
-   [ID]
-   [Label]
-   [LastModified]
-   [Macro]
-   [Mention]
-   [Parent]
-   [Space]
-   [Text]
-   [Title]
-   [Type]
-   [Watcher]

#### Ancestor

Search for all pages that are descendants of a given ancestor page. This includes direct child pages and their descendents. It is more general than the [parent] field.

###### Syntax

``` bash
ancestor
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find all descendent pages with a given anscestor page

    ``` bash
    ancestor = 123
    ```

-   Find descendants of a group of ancestor pages

    ``` bash
    ancestor in (123, 456, 789)
    ```

[^top of fields] | [^^top of topic]

#### Content

Search for content that have a given content ID. This is an alias of the [ID][1] field.

###### Syntax

``` bash
content
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None
###### Examples

-   Find content with a given content id

    ``` bash
    content = 123
    ```

-   Find content in a set of content ids

    ``` bash
    content in (123, 223, 323)
    ```

[^top of fields] | [^^top of topic]

#### Created

Search for content that was created on, before or after a particular date (or date range).

Note: search results will be relative to your configured time zone (which is by default the Confluence server's time zone)

Use one of the following formats:

`"yyyy/MM/dd HH:mm"`
`"yyyy-MM-dd HH:mm"`
`"yyyy/MM/dd"`
`"yyyy-MM-dd"`

###### Syntax

``` bash
created
```

###### Field Type

DATE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [endOfDay()]
-   [endOfMonth()]
-   [endOfWeek()]
-   [endOfYear()]
-   [startOfDay()]
-   [startOfMonth()]
-   [startOfWeek()]
-   [startOfYear()]

###### Examples

-   Find content created after the 1st September 2014

    ``` bash
    created > 2014/09/01
    ```

-   Find content created in the last 4 weeks

    ``` bash
    created >= now("-4w")
    ```

[^top of fields] | [^^top of topic]

#### Creator

Search for content that was created by a particular user. You can search by the user's username.

###### Syntax

``` bash
creator
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Find content created by jsmith

    ``` bash
    created = jsmith
    ```

-   Find content created by john smith or bob nguyen

    ``` bash
    created in (jsmith, bnguyen)
    ```

[^top of fields] | [^^top of topic]

#### Contributor

Search for content that was created or edited by a particular user. You can search by the user's username.

###### Syntax

``` bash
contributor
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Find content created by jsmith

    ``` bash
    contributor = jsmith
    ```

-   Find content created by john smith or bob nguyen

    ``` bash
    contributor in (jsmith, bnguyen)
    ```

[^top of fields] | [^^top of topic]

#### Favourite, favorite

Search for content that was favourited by a particular user. You can search by the user's username.

Due to security restrictions you are only allowed to filter on the logged in user's favourites. This field is available in both British and American spellings.

###### Syntax

``` bash
favourite
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Find content that is favourited by the current user

    ``` bash
    favourite = currentUser()
    ```

-   Find content favourited by jsmith, where jsmith is also the logged in user

    ``` bash
    favourite = jsmith
    ```

[^top of fields] | [^^top of topic]

#### ID

Search for content that have a given content ID.

###### Syntax

``` bash
id
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find content with the id 123

    ``` bash
    id = 123
    ```

-   Find content in a set of content ids

    ``` bash
    id in (123, 223, 323)
    ```

[^top of fields] | [^^top of topic]

#### Label

Search for content that has a particular label

###### Syntax

``` bash
label
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find content that has the label finished

    ``` bash
    label = finished
    ```

-   Find content that doesn't have the label draft or review

    ``` bash
    label not in (draft, review)
    ```

[^top of fields] | [^^top of topic]

#### LastModified

Search for content that was last modified on, before, or after a particular date (or date range).

The search results will be relative to your configured time zone (which is by default the Confluence server's time zone)

Use one of the following formats:

`"yyyy/MM/dd HH:mm"`
`"yyyy-MM-dd HH:mm"`
`"yyyy/MM/dd"`
`"yyyy-MM-dd"`

###### Syntax

``` bash
lastmodified
```

###### Field Type

DATE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [endOfDay()]
-   [endOfMonth()]
-   [endOfWeek()]
-   [endOfYear()]
-   [startOfDay()]
-   [startOfMonth()]
-   [startOfWeek()]
-   [startOfYear()]

###### Examples

-   Find content that was last modified on 1st September 2014

    ``` bash
    lastmodified = 2014-09-01
    ```

-   Find content that was last modified before the start of the year

    ``` bash
    lastmodified < startOfYear()
    ```

-   Find content that was last modified on or after 1st September but before 9am on 3rd September 2014

    ``` bash
    lastmodified >= 2014-09-01 and lastmodified < "2014-09-03 09:00"
    ```

[^top of fields] | [^^top of topic]

#### Macro

Search for content that has an instance of the macro with the given name in the body of the content

###### Syntax

``` bash
macro
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content that has the JIRA issue macro

    ``` bash
    macro = jira
    ```

-   Find content that has Table of content macro or the widget macro

    ``` bash
    macro in (toc, widget)
    ```

[^top of fields] | [^^top of topic]

#### Mention

Search for content that mentions a particular user. You can search by the user's username.

###### Syntax

``` bash
mention
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Find content that mentions jsmith or kjones

    ``` bash
    mention in (jsmith, kjones)
    ```

-   Find content that mentions jsmith

    ``` bash
    mention = jsmith
    ```

[^top of fields] | [^^top of topic]

#### Parent

Search for child content of a particular parent page

###### Syntax

``` bash
parent
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

###### Examples

-   Find child pages of a parent page with ID 123

    ``` bash
    parent = 123
    ```

[^top of fields] | [^^top of topic]

#### Space

Search for content that is in a particular Space. You can search by the space's key.

###### Syntax

``` bash
space
```

###### Field Type

SPACE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content in the development space or the QA space

    ``` bash
    space in (DEV, QA)
    ```

-   Find content in the development space

    ``` bash
    space = DEV
    ```

[^top of fields] | [^^top of topic]

#### Text

This is a "master-field" that allows you to search for text across a number of other text fields. These are the same fields used by Confluence's search user interface.

-   <a href="http://developer.atlassian.com/#Title" class="external-link">Title</a>
-   Content body
-   <a href="http://developer.atlassian.com/#Labels" class="external-link">Labels</a>

Note: [Confluence text-search syntax] can be used with this field.

###### Syntax

``` bash
text
```

###### Field Type

TEXT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content that contains the word Confluence

    ``` bash
    text ~ Confluence
    ```

-   Find content in the development space

    ``` bash
    space = DEV
    ```

[^top of fields] | [^^top of topic]

#### Title

Search for content by title, or with a title that contains particular text.

Note: [Confluence text-search syntax] can be used with this fields when used with the [CONTAINS] operator ("~", "!~")

###### Syntax

``` bash
title
```

###### Field Type

TEXT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content with the title "Advanced Searching using CQL"

    ``` bash
    title = "Advanced Searching using CQL"
    ```

-   Find content that matches Searching CQL (i.e. a "fuzzy" match):

    ``` bash
    title ~ "Searching CQL"
    ```

[^top of fields] | [^^top of topic]

#### Type

Search for content of a particular type. Supported content types are:

-   page
-   blogpost
-   comment
-   attachment

###### Syntax

``` bash
type
```

###### Field Type

TYPE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find blogposts or pages

    ``` bash
    type IN (blogpost, page)
    ```

-   Find attachments

    ``` bash
    type = attachment
    ```

[^top of fields] | [^^top of topic]

#### Watcher

Search for content that a particular user is watching. You can search by the user's username.

###### Syntax

``` bash
watcher
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
<td><p><img src="/confcloud/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /></p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()]

###### Examples

-   Search for content that you are watching:

    ``` bash
    watcher = currentUser()
    ```

-   Search for content that the user "jsmith" is watching:

    ``` bash
    watcher = "jsmith"
    ```

[^top of fields] | [^^top of topic]

  [operator]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-operator
  [functions]: #CQLFieldReference-function
  [Ancestor]: #CQLFieldReference-ancestorAncestorAncestor
  [Content]: #CQLFieldReference-contentContentContent
  [Created]: #CQLFieldReference-createdCreatedCreated
  [Creator]: #CQLFieldReference-creatorcreatorCreator
  [Contributor]: #CQLFieldReference-contributorcontributorContributor
  [Favourite, favorite]: #CQLFieldReference-favouriteFavouriteFavourite,favorite
  [ID]: #CQLFieldReference-idIDID
  [Label]: #CQLFieldReference-labelLabelLabel
  [LastModified]: #CQLFieldReference-lastmodifiedLastModifiedLastModified
  [Macro]: #CQLFieldReference-macroMacroMacro
  [Mention]: #CQLFieldReference-mentionMentionMention
  [Parent]: #CQLFieldReference-parentParentParent
  [Space]: #CQLFieldReference-spaceSpaceSpace
  [Text]: #CQLFieldReference-textTextText
  [Title]: #CQLFieldReference-titleTitleTitle
  [Type]: #CQLFieldReference-titleTitleType
  [Watcher]: #CQLFieldReference-Watcher
  [parent]: https://developer.atlassian.com/display/CONFDEV/CQL+Field+Reference#parent
  [^top of fields]: #CQLFieldReference-fields
  [^^top of topic]: #CQLFieldReference-top
  [1]: https://developer.atlassian.com/display/CONFDEV/CQL+Field+Reference#ID
  [endOfDay()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfDay
  [endOfMonth()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfMonth
  [endOfWeek()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfWeek
  [endOfYear()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-endOfYear
  [startOfDay()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfDay
  [startOfMonth()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfMonth
  [startOfWeek()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfWeek
  [startOfYear()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-startOfYear
  [currentUser()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-currentUser()
  [Confluence text-search syntax]: https://developer.atlassian.com/display/CONFDEV/Performing+text+searches+using+CQL
  [CONTAINS]: https://developer.atlassian.com/display/CONFDEV/Advanced+Searching+using+CQL#AdvancedSearchingusingCQL-CONTAINS:~

