---
title: In App Notifications and Tasks API 39985860
aliases:
    - /confcloud/in-app-notifications-and-tasks-api-39985860.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985860
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985860
confluence_id: 39985860
platform:
product:
category:
subcategory:
---
# Confluence Connect : In-app Notifications and Tasks API

Confluence has a REST API for [Cloud] and [Server] that's progressively replacing our existing APIs. We recommend plugin developers use the new REST APIs where possible.

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 4.3 and later.</p></td>
</tr>
<tr class="even">
<td><p>Changed:</p></td>
<td><p>In Confluence 4.3.3 we announced full support for the in-app notifications and tasks APIs - the API is no longer experimental. This release also introduces better internationalisation support for plugins.</p></td>
</tr>
</tbody>
</table>

 

Confluence 4.3 introduced the workbox, for managing notifications in the app. This includes REST and Java APIs for interacting programmatically with the in-app notifications and tasks.

Here are some resources to help you get started.

## Reference documentation

Generated reference guides:

-   <a href="http://docs.atlassian.com/mywork-confluence-host-plugin/REST/" class="external-link">Confluence Notifications and Tasks REST API</a>
-   <a href="http://docs.atlassian.com/mywork-api/" class="external-link">Confluence Notifications and Tasks Java API</a>

## Sample code

Tutorials and examples:

-   [Posting Notifications in Confluence]
-   <a href="https://bitbucket.org/cofarrell/mywork-example" class="external-link">A sample project using notifications and tasks</a>

## Giving us your feedback

You can provide feedback for the API by raising an issue in the <a href="https://jira.atlassian.com/browse/CONF" class="external-link">Confluence project on our issue tracker</a>.

  [Cloud]: /confcloud/confluence-rest-api-39985291.html
  [Server]: https://developer.atlassian.com/display/CONFDEV/Confluence+Server+REST+API
  [Posting Notifications in Confluence]: https://developer.atlassian.com/display/CONFDEV/Posting+Notifications+in+Confluence

