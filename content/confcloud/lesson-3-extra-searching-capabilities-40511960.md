---
title: Lesson 3 Extra Searching Capabilities 40511960
aliases:
    - /confcloud/lesson-3-extra-searching-capabilities-40511960.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40511960
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40511960
confluence_id: 40511960
platform:
product:
category:
subcategory:
---
# Confluence Connect : Lesson 3 - Extra Searching Capabilities

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>Add extra searching capabilities to our Customer content type</td>
</tr>
<tr class="even">
<td>Level</td>
<td><div class="content-wrapper">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td>Estimated Time</td>
<td>15 minutes</td>
</tr>
<tr class="even">
<td>Example</td>
<td><a href="https://bitbucket.org/atlassianlabs/confluence-custom-content-example" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/confluence-custom-content-example</a></td>
</tr>
</tbody>
</table>

# Prerequisites

Ensure you have worked through [Lesson 2 - Adding Content - Customers Ahoy] in the Custom content series.

# Search Integration

Up till this point, we have worked on allowing our simple system to create new customers. Let's now leverage the strong integration custom content types have with Confluence search so that we can find our customers easily!

This can be done in a very convenient way. We tell Confluence to index our custom content by declaring the **indexing.enabled** property in descriptor. 

``` bash
{
  "uiSupport": {
    "contentViewComponent": {
      "moduleKey": "customersViewer"
    },
    "listViewComponent" : {
      "moduleKey": "customersViewer"
    },
    "icons": {
      "item": {
        "url": "/images/conversations.png"
      }
    }
  },
  "apiSupport": {
    "supportedContainerTypes": [
      "space"
    ],
    "supportedChildTypes" : [
      "ac:confluence-custom-content-example:note"
    ],
    "indexing" : {
      "enabled" : true
    }
  },
  "name": {
    "value": "Customers"
  },
  "key": "customers"
}
```

By setting this property to true, Confluence will index the title and body of a piece of custom content.

Let's have a look and see if it works. Suppose we have added a customer like following:

 

<img src="/confcloud/attachments/40511960/40512123.png" class="image-center" width="488" height="250" />

 

Searching for 'NASA' yields the following:

 

<img src="/confcloud/attachments/40511960/40512125.png" class="image-center" width="832" height="250" />

 

Voila! The search is working without any extra configuration.

# Custom Text In Search Results

We can go one step further and supply excerpt text in our search results. This will make our search result a bit more specific and useful.

The magic happens in a content property called **'ac:custom-content:search-body'**, where you can store a short description for a piece of custom content. The value of this content property will be displayed in search result. You can create a new content property after a piece of Customer content is created. An alternative way of creating content property is via the metadata field in the content POST request. Please refer to <a href="https://docs.atlassian.com/atlassian-confluence/REST/latest/#content-createContent" class="external-link">Create Content REST API</a> for more information.

For the purposes of this tutorial, we use the Customer description as excerpt text - as shown below. 

``` bash
// Store content excerpt to ac:custom-content:search-body.
// We assume all required details are stored in a JSON object, 'data'.
request({
  url: "/rest/api/content/" + customer.id + "/property",
  type: "POST",
  contentType: "application/json",
  data: JSON.stringify({
    "key" : "ac:custom-content:search-body",
    "value": data.description
  }),
  success: function(response){
    //...
  },
  error: function(err){
    //...
  }
});
```

 

Now the description of new customer is showing in the search result. Nice and easy!

 

<img src="/confcloud/attachments/40511960/40511981.png" class="image-center" width="552" height="250" />
 <img src="/confcloud/attachments/40511960/40511980.png" class="image-center" width="621" height="250" />

 

Further more, your customized excerpt text is now indexed by Confluence as well. Users are able to find their content more quickly and easily.

What's next? Head over to [Lesson 4 - Noting Down Information]. We will build a nested content type! 

 

  [Lesson 2 - Adding Content - Customers Ahoy]: /confcloud/lesson-2-adding-content-customers-ahoy-40511959.html
  [Lesson 4 - Noting Down Information]: /confcloud/lesson-4-noting-down-information-40511961.html

