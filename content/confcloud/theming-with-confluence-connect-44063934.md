---
title: Theming with Confluence Connect 44063934
aliases:
    - /confcloud/theming-with-confluence-connect-44063934.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44063934
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44063934
confluence_id: 44063934
platform:
product:
category:
subcategory:
---
# Confluence Connect : Theming with Confluence Connect

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>Change the look and feel of all content, or content within a specific space.</td>
</tr>
<tr class="even">
<td>Level</td>
<td><div class="content-wrapper">
<p>3 - INTERMEDIATE</p>
</div></td>
</tr>
<tr class="odd">
<td>Estimated Time</td>
<td>15 minutes</td>
</tr>
</tbody>
</table>

 

Theming lets you change the look and feel of Confluence. Your theme can change styling for all spaces on a Confluence site, or be applied on a space-by-space basis. You can also choose to affect certain aspects within a space, like the space home page, other pages and blog posts, and the Confluence header.

Themes aren't just for show, though--they can help emphasize parts of the UI, and guide and influence user behavior in Confluence. When you're designing a theme for commercial use, it's a good idea to define its purpose and understand how you'd like users to experience it.

# Prerequisites

Before you start, make sure you've worked through the [Quick Start Guide for Confluence Connect].

# Create an empty Connect theme

First, we'll set up a Connect project and define a Confluence Connect theme. By the end of this section you won't yet have a functioning theme (we'll get to that), but you'll understand the basics of how a theme is defined.

1.  Create a new Connect add-on with <a href="https://www.npmjs.com/package/atlas-connect" class="external-link">atlas-connect</a> and install the required npm dependencies.
    Enter the following in the command line:

    ``` bash
    atlas-connect new example-connect-theme
    cd example-connect-theme
    npm install
    ```

2.  Add a new Confluence theme to your Connect descriptor:

    **atlassian-connect.json**

    ``` bash
    "confluenceThemes": [{
        "key": "my-first-theme",
        "icon": {
            "width": 110,
            "height": 70,
            "url":"/icon.png"
        },
        "name": {
            "value": "Hello World Theme"
        },
        "description": {
            "value": "My first Connect theme."
        },
        "routes": {
            "spaceview": {
                "url": "/space/{space.key}"
            }
        }
    }]
    ```

3.  Start your add-on with `npm start` in the command line

Your theme will now appear on the Confluence admin themes page. Head to ![] &gt; **General configuration** &gt; **Themes** to find it.

<img src="/confcloud/attachments/44063934/44063946.png" width="719" height="400" />

Nice work! You've just created a Confluence theme.

# Override the space home page (optional)

You can replace the space home page with a completely different experience. Rather than displaying a regular Confluence page when users visit a space using your theme, you can define an experience more suited to your theme.

To do this, we'll tell Confluence, when this theme is enabled, load the URL `/space/{space.key}` from your Connect add-on.

### Create a route handler

We previously added this snippet in the connect module:

**atlassian-connect.json**

``` bash
"routes": {
    "spaceview": {
        "url": "/space/{space.key}"
    }
}
```

Now, we're going to create a route handler to handle requests to `/space/{space.key}`.

 

1.  Open `routes/index.js` and add the following route handler to replace the space home page with a generic "Hello World" page

    **routes/index.js**

    ``` bash
    app.get('/space/:spaceKey', addon.authenticate(), function (req, res) {
            res.render('hello-world', {
                title: 'Atlassian Connect'
            });
        }
    );
    ```

2.  Restart your add-on with `npm start` in the command line
    To make sure everything's working up to this point, do either of the following:
     

    -   Enable the theme for the whole site - Go to ![] &gt; **General configuration** &gt; **Themes** and select your theme
    -   Enable the theme for one space - Go to the space, choose **Space tools** &gt; **Look and Feel** &gt; **Themes** and select your theme

    Go to a space the theme is applied to, and you should now see this page:

    <img src="/confcloud/attachments/44063934/44063949.png" height="400" />
    Next, we create the new space home page. We'll make our own space home override by adding a new template file to `view`, Javascript file to `public/js`, and stylesheet file to `public/css. `

3.  Add the file `view/my-space-home.hbs` with the following:

    **view/my-space-home.hbs**

    ``` bash
    {{!< layout}}
    <div id="main">
        <div class="main-container">
            <div class="search-area">
                <h1 class="headings">{{ title }}</h1>
                <input type="text" class="search-box" placeholder="Search for content within this space" />
            </div>
            <div class="content">
                <p class="newspaper">
                    <span class="title">Lorem ipsum</span><br> dolor sit amet, consectetur adipiscing elit.
                    Ut in nulla vitae elit faucibus lobortis at vel nibh.
                </p>
            </div>
        </div>
    </div>
    ```

4.  Change `routes/index.js` to:

    **route/index.js**

    ``` bash
    app.get('/space/:spaceKey', addon.authenticate(), function (req, res) {
            res.render('space-home', {
                title: 'Welcome to Theme Home'
            });
        }
    );
    ```

5.  Add any CSS rules for styling your space home to `public/css/addon.css`
6.  Restart your add-on with `npm start` in the command line

Make sure your theme's still enabled for your current space, then head to the space. You should see your new space home page.

<img src="/confcloud/attachments/44063934/44063950.png" height="400" />

 

From here, it's up to you to decide what your space home experience is like.

# Change the look and feel of pages, blog posts, and the Confluence header

To do this, you need to provide a new property in your confluenceThemes module called "lookAndFeel":

**atlassian-connect.json**

``` bash
"confluenceThemes": [{
    "key": "my-first-theme",
    "icon": {
        "width":110,
        "height": 70,
        "url":"/icon.png"
    },
    "name": {
        "value": "Hello World Theme"
    },
    "description": {
        "value": "My first Connect theme."
    },
    "routes": {
        "spaceview": {
            "url": "/space/{space.key}"
        }
    },
    "lookAndFeel": {
        // Your look and feel settings go here.
    }
}]
```

Here's an example of a theme that specifies a black header:

**atlassian-connect.json**

``` bash
"confluenceThemes": [{
    ...
    "lookAndFeel": {
        "header": {
            "backgroundColor": "#000000"
                ...
        }
        ...
    }
}]
```

The lookAndFeel properties aren't mandatory. If you don't specify a value, default values are used.

### Available look and feel properties

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>Setting</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>lookAndFeel.header.backgroundColor</code></td>
<td><div class="content-wrapper">
<p>Changes the background color of the Confluence header.</p>
<p>Example with the value set to <code>#666666</code>:</p>
<p><img src="/confcloud/attachments/44063934/44063951.png" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td><p><code>lookAndFeel.header.primaryNavigation.hoverOrFocus.backgroundColor</code></p></td>
<td><div class="content-wrapper">
<p>Changes the background color when hovering menu items in the Confluence header.</p>
<p>Example with the value set to <code>#FF8C00</code>:</p>
<p><img src="/confcloud/attachments/44063934/44063952.png" height="250" /></p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.header.primaryNavigation.hoverOrFocus.color</code></td>
<td><div class="content-wrapper">
<p>Changes the text color when hovering menu items in the Confluence header.</p>
<p>Example with value set to <code>#FFFF00</code>:</p>
<p><img src="/confcloud/attachments/44063934/44063953.png" /></p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.header.primaryNavigation.color</code></td>
<td><div class="content-wrapper">
<p>Changes the text color for menu items in the Confluence header.</p>
<p>Example with the value set to <code>#000000</code>:</p>
<p><img src="/confcloud/attachments/44063934/44063954.png" height="250" /></p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.header.secondaryNavigation.hoverOrFocus.backgroundColor</code></td>
<td><div class="content-wrapper">
<p>Changes the background color when hovering drop-down menus in the Confluence header.</p>
<p>Example with the value set to &quot;#FFFF00&quot;:</p>
<p><img src="/confcloud/attachments/44063934/44063955.png" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.header.secondaryNavigation.hoverOrFocus.color</code></td>
<td><div class="content-wrapper">
<p>Changes the text color when hovering drop-down menus in the Confluence header.</p>
<p>Example with the value set to &quot;#000000&quot;:</p>
<p><img src="/confcloud/attachments/44063934/44063955.png" height="250" /></p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.header.secondaryNavigation.color</code></td>
<td><div class="content-wrapper">
<p>Changes the text color of items in drop-down menus that extend from the Confluence header.</p>
<p>Example with the value set to &quot;#999999&quot;:</p>
<p><img src="/confcloud/attachments/44063934/44063955.png" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.header.search.backgroundColor</code></td>
<td><div class="content-wrapper">
<p>Changes the background color of the search box in the Confluence header.</p>
<p>Example with the value set to &quot;#CC0000&quot;:</p>
<p><img src="/confcloud/attachments/44063934/44063956.png" /></p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.header.search.color</code></td>
<td><div class="content-wrapper">
<p>Changes the text color of the search box in the Confluence header.</p>
<p>Example with the value set to &quot;#FFFFFF&quot;:</p>
<p><img src="/confcloud/attachments/44063934/44063956.png" /></p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.header.button.backgroundColor</code></td>
<td><div class="content-wrapper">
<p>Changes the background color of the Create button in the Confluence header.</p>
<p>Example with the value set to &quot;#FF8C00&quot;:</p>
<p><img src="/confcloud/attachments/44063934/44063957.png" height="250" /></p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.header.button.color</code></td>
<td><div class="content-wrapper">
<p>Changes the text color of the <strong>Create</strong> button in the Confluence header. Currently not applied to the ellipsis <img src="https://pug.jira-dev.com/wiki/download/thumbnails/2317647937/ellipsis.png?version=1&amp;modificationDate=1478213167701&amp;cacheVersion=1&amp;api=v2" class="confluence-thumbnail confluence-external-resource" width="22" />.</p>
<p>Example with the value set to &quot;#CC0000&quot;.</p>
<p><img src="/confcloud/attachments/44063934/44063958.png" height="250" /><br />
</p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.headings.color</code></td>
<td><div class="content-wrapper">
<p>Applies to HTML H1 to H6 tags.</p>
<p>Example with the value set to &quot;#990000&quot;.</p>
<p><img src="/confcloud/attachments/44063934/44063959.png" height="250" /></p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.links.color</code></td>
<td><div class="content-wrapper">
<p>Applies to a variety of anchors.</p>
<p>Example with the value set to &quot;#FF8C00&quot;.</p>
<p><img src="/confcloud/attachments/44063934/44063960.png" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.menus.hoverOrFocus.backgroundColor</code></td>
<td><div class="content-wrapper">
<p>Changes the background color of drop-down menus inside pages when hovering. For example, the network macro autocomplete menu.</p>
<p>Example with the value set to &quot;#C99366&quot;.</p>
<p><img src="/confcloud/attachments/44063934/44063961.png" height="250" /></p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.menus.color</code></td>
<td><div class="content-wrapper">
<p>Changes the text color of drop-down menus. For example, the more actions menu <img src="https://pug.jira-dev.com/wiki/download/thumbnails/2317647937/ellipsis.png?version=1&amp;modificationDate=1478213167701&amp;cacheVersion=1&amp;api=v2" class="confluence-thumbnail confluence-external-resource" width="22" /> and the <strong>Space tools</strong> menu.</p>
<p>Example with the value set to &quot;#722837&quot;.</p>
<p><img src="/confcloud/attachments/44063934/44063962.png" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.content.screen.gutterTop</code></td>
<td><div class="content-wrapper">
<p>Modifies the gutter between the content for pages and blog posts and the Confluence header.</p>
<p>Allowed values are: &quot;none&quot;, &quot;default&quot;, height (in pixels), and percentage.</p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.content.screen.gutterRight</code></td>
<td><div class="content-wrapper">
<p>Modifies the gutter on the right of the content for pages and blog posts.</p>
<p>Allowed values are: &quot;none&quot;, &quot;default&quot;, &quot;small&quot;, &quot;medium&quot;, and &quot;large&quot;.</p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.content.screen.gutterBottom</code></td>
<td><div class="content-wrapper">
<p>Modifies the gutter between the content for pages and blog posts and the footer.</p>
<p>Allowed values are: &quot;none&quot;, &quot;default&quot;, height (in pixels), and percentage.</p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.content.screen.gutterLeft</code></td>
<td><div class="content-wrapper">
<p>Modifies the gutter on the left of the content for pages and blog posts.</p>
<p>Allowed values are: &quot;none&quot;, &quot;small&quot;, &quot;medium&quot;, and &quot;large&quot;.</p>
<p>Here's an example with <code>gutterLeft</code> set to &quot;medium&quot;, <code>gutterTop</code> set to &quot;20px&quot;, <code>gutterRight</code> set to &quot;small&quot;, and <code>gutterBottom</code> set to &quot;none&quot;.</p>
<p><img src="/confcloud/attachments/44063934/44063963.png" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td><p><code>lookAndFeel.content.screen.background</code></p>
<p><code>lookAndFeel.content.screen.background-color</code></p>
<p><code>lookAndFeel.content.screen.background-image</code></p>
<p><code>lookAndFeel.content.screen.background-repeat</code></p>
<p><code>lookAndFeel.content.screen.background-size</code></p>
<p><code>lookAndFeel.content.screen.background-clip</code></p>
<p><code>lookAndFeel.content.screen.background-attachment</code></p>
<p><code>lookAndFeel.content.screen.background-origin</code></p>
<p><code>lookAndFeel.content.screen.background-position</code></p>
<p><code>lookAndFeel.content.screen.background-blend-mode</code></p></td>
<td><div class="content-wrapper">
<p>Modifies the background of Confluence content. All CSS3 background properties found in the <a href="https://developer.mozilla.org/en/docs/Web/CSS/background" class="external-link">MDN Reference</a> are supported.</p>
<p>Example with the <code>background</code> property set to &quot;linear-gradient(45deg, rgba(176,104,112,1) 0%, rgba(244,212,216,1) 100%)&quot;.</p>
<p><img src="/confcloud/attachments/44063934/44063964.png" height="250" /></p>
</div></td>
</tr>
<tr class="odd">
<td><p><code>lookAndFeel.content.screen.layer.background</code></p>
<p><code>lookAndFeel.content.screen.layer.background-color</code></p>
<p><code>lookAndFeel.content.screen.layer.background-image</code></p>
<p><code>lookAndFeel.content.screen.layer.background-repeat</code></p>
<p><code>lookAndFeel.content.screen.layer.background-size</code></p>
<p><code>lookAndFeel.content.screen.layer.background-clip</code></p>
<p><code>lookAndFeel.content.screen.layer.background-attachment</code></p>
<p><code>lookAndFeel.content.screen.layer.background-origin</code></p>
<p><code>lookAndFeel.content.screen.layer.background-position</code></p>
<p><code>lookAndFeel.content.screen.layer.background-blend-mode</code></p></td>
<td><div class="content-wrapper">
<p>Modifies the background of the layer behind Confluence content and above the screen. All CSS3 background properties found in the <a href="https://developer.mozilla.org/en/docs/Web/CSS/background" class="external-link">MDN Reference</a> are supported.</p>
<p>Example with the <code>background</code> property set to &quot;{url('/img/sunset-beach.jpg') no-repeat 0 0/cover&quot; (a screen layer width of &quot;250px&quot; and height of &quot;100%&quot; has also been set).</p>
<p><img src="/confcloud/attachments/44063934/44063965.png" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td><p><code>lookAndFeel.content.screen.layer.width</code></p>
<p><code>lookAndFeel.content.screen.layer.height</code></p></td>
<td><div class="content-wrapper">
<p>Modifies the width and height of the layer behind Confluence content and above the screen.</p>
<p>Allowed values are: length (in pixels) and percentage.</p>
</div></td>
</tr>
<tr class="odd">
<td><p><code>lookAndFeel.content.container.background</code></p>
<p><code>lookAndFeel.content.container.background-color</code></p>
<p><code>lookAndFeel.content.container.background-image</code></p>
<p><code>lookAndFeel.content.container.background-repeat</code></p>
<p><code>lookAndFeel.content.container.background-size</code></p>
<p><code>lookAndFeel.content.container.background-clip</code></p>
<p><code>lookAndFeel.content.container.background-attachment</code></p>
<p><code>lookAndFeel.content.container.background-origin</code></p>
<p><code>lookAndFeel.content.container.background-position</code></p>
<p><code>lookAndFeel.content.container.background-blend-mode</code></p></td>
<td><div class="content-wrapper">
<p>The container refers to the surrounding element for the content header, content body, and comments. This background style will apply to the container if no background values are specified for header or body.</p>
<p>Example with the value set to &quot;rgba(0, 0, 0, 0.2)&quot; (a container padding of &quot;40px&quot; and border radius of &quot;10px&quot; has also been set):</p>
<p><img src="/confcloud/attachments/44063934/44063966.png" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.content.container.padding</code></td>
<td><div class="content-wrapper">
<p>Applies padding to the container element. This padding style will apply to the container if no padding values are specified for header or body.</p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.content.container.borderRadius</code></td>
<td><div class="content-wrapper">
<p>Changes the border radius of the container element. This border radius style will apply to the container if no border radius values are specified for the header or body.</p>
</div></td>
</tr>
<tr class="even">
<td><p><code>lookAndFeel.content.header.background</code></p>
<p><code>lookAndFeel.content.header.background-color</code></p>
<p><code>lookAndFeel.content.header.background-image</code></p>
<p><code>lookAndFeel.content.header.background-repeat</code></p>
<p><code>lookAndFeel.content.header.background-size</code></p>
<p><code>lookAndFeel.content.header.background-clip</code></p>
<p><code>lookAndFeel.content.header.background-attachment</code></p>
<p><code>lookAndFeel.content.header.background-origin</code></p>
<p><code>lookAndFeel.content.header.background-position</code></p>
<p><code>lookAndFeel.content.header.background-blend-mode</code></p></td>
<td><div class="content-wrapper">
<p>Modifies the background of the content header. All CSS3 background properties found in the <a href="https://developer.mozilla.org/en/docs/Web/CSS/background" class="external-link">MDN Reference</a> are supported.</p>
<p>This is an example with the value set to &quot;rgba(0, 0, 0, 0.2)&quot; (a content header padding of &quot;20px 0 90px 20px&quot; and border radius of &quot;5px 5px 0 0&quot; has also been set)</p>
<p><img src="/confcloud/attachments/44063934/44063967.png" height="250" /></p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.content.header.padding</code></td>
<td>Changes the padding for the content header.</td>
</tr>
<tr class="even">
<td><code>lookAndFeel.content.header.borderRadius</code></td>
<td><p>Changes the border radius of the content header.</p></td>
</tr>
<tr class="odd">
<td><p><code>lookAndFeel.content.body.background</code></p>
<p><code>lookAndFeel.content.body.background-color</code></p>
<p><code>lookAndFeel.content.body.background-image</code></p>
<p><code>lookAndFeel.content.body.background-repeat</code></p>
<p><code>lookAndFeel.content.body.background-size</code></p>
<p><code>lookAndFeel.content.body.background-clip</code></p>
<p><code>lookAndFeel.content.body.background-attachment</code></p>
<p><code>lookAndFeel.content.body.background-origin</code></p>
<p><code>lookAndFeel.content.body.background-position</code></p>
<p><code>lookAndFeel.content.body.background-blend-mode</code></p></td>
<td><div class="content-wrapper">
<p>Modifies the background of the content body and comments. All CSS3 background properties found in the <a href="https://developer.mozilla.org/en/docs/Web/CSS/background" class="external-link">MDN Reference</a> are supported.</p>
<p>Example with the background property set to &quot;url('/img/skyline-pink-grey.jpg') no-repeat center bottom&quot; (a content body padding of &quot;0 40px 40px 40px&quot; and border radius of &quot;0 0 5px 5px&quot; has also been set).</p>
<p><br />
<img src="/confcloud/attachments/44063934/44063968.png" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td><code>lookAndFeel.content.body.padding</code></td>
<td><div class="content-wrapper">
<p>Changes the padding on the content body. This property supports the CSS padding shorthand property as defined in the <a href="https://developer.mozilla.org/en/docs/Web/CSS/padding" class="external-link">MDN Reference</a>.</p>
<p>The example above shows the value set to &quot;0 40px 40px 40px&quot;.</p>
</div></td>
</tr>
<tr class="odd">
<td><code>lookAndFeel.content.body.borderRadius</code></td>
<td><div class="content-wrapper">
<p>Changes the border radius of the content body.</p>
<p>This property supports the CSS border-radius shorthand property as defined in the <a href="https://developer.mozilla.org/en/docs/Web/CSS/border-radius" class="external-link">MDN Reference</a>.</p>
<p>Example with the value set to &quot;0 0 20px 20px&quot;.</p>
<p><img src="/confcloud/attachments/44063934/44063969.png" height="250" /></p>
</div></td>
</tr>
</tbody>
</table>

## Example look and feel

Here's a complete example of a theme changing the look and feel of Confluence.

``` bash
"lookAndFeel": {
    "headings": {
        "color": "#333333"
    },
    "links": {
        "color": "#732D3E"
    },
    "menus": {
        "hoverOrFocus": {
            "backgroundColor": "#3873AE"
        },
        "color": "#732D3E"
    },
    "bordersAndDividers": {
        "color": "#0D0E0E"
    },
    "header": {
        "backgroundColor": "#661F2D",
        "button": {
            "backgroundColor": "#894E59",
            "color": "#FFFFFF"
        },
        "primaryNavigation": {
            "hoverOrFocus": {
                "backgroundColor": "#863647",
                "color" : "#FFFFFF"
            },
            "color": "#FFFFFF"
        },
        "secondaryNavigation": {
            "hoverOrFocus": {
                "backgroundColor": "#863647",
                "color": "#FFFFFF"
            },
            "color": "#000000"
        },
        "search": {
            "backgroundColor": "#9A636B",
            "color": "#FFFFFF"
        }
    },
    "content": {
        "screen": {
            "layer": {
                "height": "250px",
                "width": "100%",
                "backgroundImage": "url('//img/skyline_pink.jpg')",
                "backgroundSize": "cover",
                "backgroundRepeat": "no-repeat"
            },
            "background": "linear-gradient(45deg, rgba(176,104,112,1) 0%, rgba(244,212,216,1) 100%)",
            "gutterTop": "20px",
            "gutterRight": "small",
            "gutterBottom": "none",
            "gutterLeft": "small"
        },
        "container": {
            "background": "#F4D4D8",
            "padding": "0 20px",
            "borderRadius": "10px"
        },
        "header": {
            "background": "rgba(0, 0, 0, 0.2)",
            "padding": "20px 0  90px 20px",
            "borderRadius": "5px 5px 0 0"
        },
        "body": {
            "background": "#FFFFFF",
            "padding": "10px",
            "borderRadius": "0 0 5px 5px"
        }
    }
}
```

 

# Wrap up

Once you've completed styling your theme, that's it! Time to make it available and get people using it. To do this, take a look at our pages on [Installing through the Atlassian Marketplace] and [Selling on the Atlassian Marketplace].

# Further reading

-   [Understanding Atlassian in the cloud]
-   [Upgrading and versioning your add-on]
-   [Confluence Connect patterns]

  [Quick Start Guide for Confluence Connect]: /confcloud/quick-start-to-confluence-connect-39987884.html
  []: /confcloud/attachments/44063934/44063938.png
  [1]: /confcloud/attachments/44063934/44063953.png
  [2]: /confcloud/attachments/44063934/44063956.png
  [Installing through the Atlassian Marketplace]: https://developer.atlassian.com/static/connect/docs/latest/developing/cloud-installation.html
  [Selling on the Atlassian Marketplace]: https://developer.atlassian.com/static/connect/docs/latest/developing/selling-on-marketplace.html
  [Understanding Atlassian in the cloud]: https://developer.atlassian.com/static/connect/docs/latest/concepts/cloud-development.html
  [Upgrading and versioning your add-on]: https://developer.atlassian.com/static/connect/docs/latest/developing/upgrades.html
  [Confluence Connect patterns]: /confcloud/confluence-connect-patterns-39981569.html

