#!/bin/bash

set -ex

if [ $1 != "linux_386" ] && [ $1 != "linux_amd64" ] && \
   [ $1 != "darwin_386" ] && [ $1 != "darwin_amd64" ] && \
   [ $1 != "windows_386" ] && [ $1 != "windows_amd64" ]; then
    echo "Supply an OS and ARCH to build to in format GOOS_GOARCH"
    echo "Supported values: darwin_386, linux_386, windows_386, darwin_amd64, linux_amd64, windows_amd64"
    exit -1
fi

GOOS=${1/_*/}
GOARCH=${1/*_/}

REPO=docker.atlassian.io/atlassian/dac-hugo-build

# GOOS: darwin linux windows
# GOARCH: 386 amd64

docker run -i --rm \
       -v `pwd`:/data \
       -e GOOS=$GOOS \
       -e GOARCH=$GOARCH \
       ${REPO} /bin/bash -c "cd /data && ./scripts/build-custom-hugo.sh"
