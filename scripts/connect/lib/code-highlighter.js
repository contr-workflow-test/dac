const ANY_TAG_UNDER_PRE_PATTERN = /(<pre[^>]*>[^<]*)<(\w+)[^>]*>([^<]*?)<\/\2>([^<]*<\/pre>)/ig;
const JAVASCRIPT_LANGUAGE = 'javascript';
const CODE_CONTENT_GROUP = 1;
const FULL_GROUP = 0;

module.exports = {
    highlightContent(value){
        var highlighted = [];
        var prePattern = /<pre[^>]*>([^]+?)<\/pre>/ig;
        var result;
        var lastIndex = 0;
        value = cleanupCodeBlocks(value);
        while ((result = prePattern.exec(value)) != null) {
            if (result.index - lastIndex != 0) {
                highlighted.push({html: value.substring(lastIndex, result.index)})
            }
            highlighted.push({code: result[CODE_CONTENT_GROUP], language: JAVASCRIPT_LANGUAGE});
            lastIndex = result.index + result[FULL_GROUP].length
        }
        if (highlighted.length == 0) {
            highlighted.push({html: value})
        }
        return highlighted
    }
};

function cleanupCodeBlocks(content) {
    return content.replace(ANY_TAG_UNDER_PRE_PATTERN, '$1$3$4')
}