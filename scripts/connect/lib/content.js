var jsonfile = require('jsonfile');
var fs = require('fs-extra')
var _ = require('lodash');
var helper = require('./helper');
var marked = require('marked');
var traverse = require('traverse');
var moment = require('moment');
var codeHighlighter = require('./code-highlighter')

const CONTENT_ROOT_DIR = '../../content';
const DATA_DIR = '../../data';
const DESCRIPTION_KEY = 'description';
const SLICED_DESCRIPTION_KEY = 'sliceddescription';
const POTENTIAL_CODE_FIELDS = ['description', 'fieldDescription', 'properties', 'functions',
  'parameters', 'subParameters'];

class Content {
  constructor(title, frontMatter) {
    this.title = title;
    this.frontMatter = _.clone(frontMatter);
    if (_.has(this.frontMatter, 'name')) {
      this.frontMatter.name = this.frontMatter.name.replace("Jira", "JIRA");
    }
    this.frontMatter.date = moment().format('YYYY-MM-DD');
  }

  // This will traverse through all properties of the frontMatter 
  // and convert markdown to HTML for keys that match fields. 
  parseMarkdown(fields) {
    traverse(this.frontMatter).forEach(function (val) {
      if (_.includes(fields, this.key) && typeof val === 'string') {
        this.update(marked(val));
      }
    })
  }

  write(section, meta) {
    if (!section) {
      throw new Error('Section and sub sections(s) must be provided.');
    }

    // section deeper than one level must be joined to a string format
    if (section instanceof Array) {
      section = section.join('/');
    }

    // final destination of the content file
    var dir = `${CONTENT_ROOT_DIR}/${section}`;

    var thisContent = this;

    // ensure that the write directory exists, make one if it's not there
    fs.ensureDir(dir, function (dirError) {
      if (dirError) {
        console.error(`Could not create ${dir}`);
      } else {
        jsonfile.writeFile(`${dir}/${thisContent.title}.md`, _.extend(thisContent.frontMatter, meta), { spaces: 2 }, function (writeError) {
          if (writeError) {
            console.error(`Could not write ${thisContent.title} to ${dir}`);
            console.error(writeError);
          } else {
            console.log(`Wrote content ${thisContent.title}.md to ${dir}`);
          }

        });
      }
    });
  }

  writeData() {
    var thisContent = this;
    fs.ensureDir(DATA_DIR, function (dirError) {
      if (dirError) {
        console.error(`Could not create ${DATA_DIR}`);
      } else {
        jsonfile.writeFile(`${DATA_DIR}/${thisContent.title}.json`, thisContent.frontMatter, { spaces: 2 }, function (writeError) {
          if (writeError) {
            console.error(`Could not write ${thisContent.title} to ${DATA_DIR}`);
          } else {
            console.log(`Writing content ${thisContent.title}.md to ${DATA_DIR}`);
          }

        });
      }
    });
  }

  highlightCode() {
    this._highlightCode(this.frontMatter, POTENTIAL_CODE_FIELDS);
  }

  _highlightCode(data, fields) {
    Object.keys(data).forEach(key=> {
      if (_.includes(fields, key)) {
        var value = data[key];
        if (typeof value === 'string') {
          var highlighted = codeHighlighter.highlightContent(value);
          if (DESCRIPTION_KEY == key) {
            data[SLICED_DESCRIPTION_KEY] = highlighted;
          } else {
            data[key] = highlighted;
          }
        } else if (value instanceof Array) {
          value.forEach(elem=> {
            this._highlightCode(elem, fields)
          })
        }
      }
    });
  }
}

module.exports = Content;