#!/bin/bash
set -e

runonly=0
buildimage=0
daemon=0

while getopts "rbd" opt; do
    case "$opt" in
        r) runonly=1 ;;
        b) buildimage=1 ;;
        d) daemon=1 ;;
    esac
done
shift $((OPTIND-1))

if [[ $buildimage == 1 ]]; then
    echo "+++ Docker building build image..."

    BUILD_REPO=docker.atlassian.io/atlassian/dac-build
    docker build --tag ${BUILD_REPO} -f docker/dac-build.dockerfile .
    . scripts/build-site-in-docker.sh -p
fi


MICROS_REPO=docker.atlassian.io/atlassian/dac-static
VERSION=`cat release.txt`   # Saved during build phase

if [[ $runonly != 1 ]]; then
    echo "+++ Docker building micros image..."
    docker build --tag ${MICROS_REPO}:${VERSION} -f docker/micros-image.dockerfile .
fi


DOPTS="-p 0.0.0.0:8080:8080 \
       -p 0.0.0.0:8081:8081 \
       -ti"

if [[ $daemon == 1 ]]; then
    DOPTS="$DOPTS -d --name DAC"
    msg="+++ Docker running container 'DAC' Application on http://localhost:8080 ..."
else
    DOPTS="$DOPTS --rm"
fi

docker run $DOPTS \
       -e APACHE_SERVER_NAME='localhost' \
       -e DISABLE_CACHE='true' \
       ${MICROS_REPO}:${VERSION} $@

echo $msg
