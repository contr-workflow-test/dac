#!/bin/bash
set -e
if [[ -n "${bamboo_buildResultKey+set}" ]]; then
    git remote rm origin
    git remote add origin 'git@bitbucket.org:atlassian/dac-hugo-custom-builds.git'
    VERSION=`./hugo_linux_amd64 version | cut -d ' ' -f 5`

    tar --transform='s/.*/hugo/' -czvf hugo_macos_386.tar.gz hugo_darwin_386
    tar --transform='s/.*/hugo/' -czvf hugo_macos_amd64.tar.gz hugo_darwin_amd64
    tar --transform='s/.*/hugo/' -czvf hugo_linux_386.tar.gz hugo_linux_386
    tar --transform='s/.*/hugo/' -czvf hugo_linux_amd64.tar.gz hugo_linux_amd64
    tar --transform='s/.*/hugo/' -czvf hugo_win_386.tar.gz hugo_windows_386
    tar --transform='s/.*/hugo/' -czvf hugo_win_amd64.tar.gz hugo_windows_amd64

    git add hugo_macos_386.tar.gz hugo_macos_amd64.tar.gz hugo_linux_386.tar.gz hugo_linux_amd64.tar.gz hugo_win_386.tar.gz hugo_win_amd64.tar.gz
    git commit -m "Release version ${VERSION}"
    git tag -a -m "Release version ${VERSION}" ${VERSION}

    echo "+++ Pushing to origin master"
    git push origin master

    echo "+++ Pushing to tag ${VERSION}"
    git push origin ${VERSION}

fi
