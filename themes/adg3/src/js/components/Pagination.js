// taken from https://github.com/vayser/react-js-pagination
import React, { Component, PropTypes } from "react"
import paginator from "paginator"
import cx from "classnames"

const Page = ({
  pageText,
  pageNumber,
  activeClass,
  disabledClass,
  isActive,
  isDisabled,
  onClick,
  cssClass
  }) => {
  const text = pageText || pageNumber
  const css = cx({
    [activeClass]: isActive,
    [disabledClass]: isDisabled,
    [cssClass]: cssClass
  })

  if (React.isValidElement(text)) {
    return text;
  }
  const clickHandler = (e) => {
    e.preventDefault();
    onClick(pageNumber);
  }
  return (
    <li className={css}>
      {(isDisabled || isActive) ? text :
        <a onClick={clickHandler} href="#">
          { text }
        </a>
      }
    </li>
  )
}

Page.propTypes = {
  pageText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  pageNumber: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
  isDisabled: PropTypes.bool,
  activeClass: PropTypes.string,
  disabledClass: PropTypes.string,
  cssClass: PropTypes.string
}

Page.defaultProps = {
  activeClass: "current",
  disabledClass: "disabled",
  isActive: false,
  isDisabled: false
}

class Pagination extends Component {
  buildPages() {
    const pages = [];
    const {
      itemsCountPerPage,
      pageRangeDisplayed,
      activePage,
      prevPageText,
      nextPageText,
      firstPageText,
      lastPageText,
      totalItemsCount,
      totalPagesCountLimit,
      onChange,
      activeClass
      } = this.props;

    const itemsCount = (totalItemsCount > (totalPagesCountLimit * itemsCountPerPage)) ? totalPagesCountLimit * itemsCountPerPage : totalItemsCount

    const paginationInfo = new paginator(itemsCountPerPage, pageRangeDisplayed)
      .build(itemsCount, activePage)

    if (paginationInfo.first_page !== paginationInfo.last_page) {
      for(let i = paginationInfo.first_page; i <= paginationInfo.last_page; i++) {
        pages.push(
          <Page
            isActive={i === activePage}
            key={i}
            pageNumber={i}
            onClick={onChange}
            activeClass={activeClass}
            />
        )
      }
    }

    pages.unshift(
      <Page
        cssClass="pagination-previous-page"
        key={"prev" + paginationInfo.previous_page}
        pageNumber={paginationInfo.previous_page}
        onClick={onChange}
        pageText={prevPageText}
        isDisabled={!paginationInfo.has_previous_page}
        />
    )

    pages.unshift(
      <Page
        cssClass="hide-for-small-only"
        key={"first"}
        pageNumber={1}
        onClick={onChange}
        pageText={firstPageText}
        isDisabled={paginationInfo.current_page === paginationInfo.first_page}
        />
    )

    pages.push(
      <Page
        cssClass="pagination-next-page"
        key={"next" + paginationInfo.next_page}
        pageNumber={paginationInfo.next_page}
        onClick={onChange}
        pageText={nextPageText}
        isDisabled={!paginationInfo.has_next_page}
        />
    )

    pages.push(
      <Page
        cssClass="hide-for-small-only"
        key={"last"}
        pageNumber={paginationInfo.total_pages}
        onClick={onChange}
        pageText={lastPageText}
        isDisabled={paginationInfo.current_page === paginationInfo.total_pages}
        />
    )

    return pages;
  }

  render() {
    const pages = this.buildPages();
    return (
      <ul className={this.props.innerClass}>{pages}</ul>
    )
  }
}

Pagination.propTypes = {
  totalItemsCount: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  activePage: PropTypes.number,
  pageRangeDisplayed: PropTypes.number,
  itemsCountPerPage: PropTypes.number,
  totalPagesCountLimit: PropTypes.number,
  prevPageText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  nextPageText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  lastPageText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  firstPageText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  innerClass: PropTypes.string,
  activeClass: PropTypes.string,
  disabledClass: PropTypes.string
}

Pagination.defaultProps = {
  itemsCountPerPage: 10,
  pageRangeDisplayed: 5,
  totalPagesCountLimit: 10,
  activePage: 1,
  prevPageText: "Previous",
  firstPageText: "«",
  nextPageText: "Next",
  lastPageText: "»",
  innerClass: "pagination text-center"
}

export default Pagination