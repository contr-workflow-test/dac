import React, {Component, PropTypes} from "react"

const LabelLink = ({isActive, text, onClick}) => {
  if (isActive) {
    return <span className="active">{text}</span>
  }
  return (
    <a href="javascript:void(0)" onClick={onClick}>
      {text}
    </a>
  )
}

LabelLink.PropTypes = {
  isActive: PropTypes.boolean,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

const SearchLabels = ({searchLabel, searchFacets, onLabelClick}) => {
  const clickHandler = (label) => {
    if (label === searchLabel) return
    onLabelClick(label);
  }
  return (
    <div className="product-labels">
      <hr />
      <h5>PRODUCTS</h5>
      <p>
        <LabelLink isActive={searchLabel === ''} text={'All'} onClick={() => clickHandler('all')} />
      </p>
      {searchFacets != undefined && searchFacets.length > 0 && searchFacets.map(item =>
        <p key={item[0].label_with_op}>
          <LabelLink
            isActive={searchLabel === item[0].label_with_op}
            text={item[0].anchor}
            onClick={() => clickHandler(item[0].label_with_op)}
          />
        </p>
      )}
    </div>
  )
}

SearchLabels.PropTypes = {
  searchLabel: PropTypes.string.isRequired,
  searchFacets: PropTypes.array.isRequired,
  onLabelClick: PropTypes.func.isRequired
}

export default SearchLabels