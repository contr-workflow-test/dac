import React, {Component, PropTypes} from "react"
import SearchResultItem from '../components/SearchResultItem'

const SearchResult = ({ isFetching, searchInformation, searchResults, queryString }) => (
  <div>
    <hr />
    {isFetching != undefined && isFetching &&
      <h5>Loading...</h5>
    }
    {!isFetching && searchInformation && searchInformation.totalResults == 0 &&
      <p>Your search - <strong>{queryString}</strong> - did not match any documents.</p>
    }
    {searchResults != undefined && searchResults.length > 0 && searchResults.map(item =>
      <SearchResultItem key={item.cacheId} title={item.title} snippet={item.snippet} link={item.link} />
    )}
  </div>
)

SearchResult.propTypes = {
  searchResults: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  queryString: PropTypes.string.isRequired
}

export default SearchResult