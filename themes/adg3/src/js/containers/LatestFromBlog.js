import React, {Component, PropTypes} from "react";
import {connect} from "react-redux";
import {fetchRSSFeed} from "../actions";

class Post extends Component {
  render() {
    const { title, description, url, gravatar_url } = this.props
    const desc = <span dangerouslySetInnerHTML={{__html: description}} />
    return (
      <div>
        <div className="blog-post">
          <hr/>
          <div className="medium-2 columns hide-for-small-only">
            <img src={gravatar_url} />
          </div>
          <div className="medium-10 columns">
            <h3>{title}</h3>
            <p className="hide-for-small-only">{desc}</p>
            <a href={url}>Learn more &nbsp;
            <span className="fa fa-arrow-right" aria-hidden="true"></span></a>
          </div>
        </div>
      </div>
    )
  }
}

class LatestFromBlog extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { dispatch, rss } = this.props
    dispatch(fetchRSSFeed())
  }

  render() {
    const { posts, isFetching, didInvalidate, lastUpdated } = this.props
    return (
      <span>
        {isFetching != undefined && isFetching &&
          <h3>Loading...</h3>
        }
        {posts != undefined && posts.length > 0 && posts.map(item =>
          <Post key={item.url} title={item.title} description={item.description} url={item.url} gravatar_url={item.gravatar_url}/>
        )}
      </span>
    )
  }
}

LatestFromBlog.propTypes = {
  posts: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  didInvalidate: PropTypes.bool.isRequired,
  lastUpdated: PropTypes.number,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
  const {
    posts,
    isFetching,
    didInvalidate,
    lastUpdated
  } = state.rss || {
    isFetching: true,
    didInvalidate: false,
    posts: []
  }

  return {
    posts,
    isFetching,
    didInvalidate,
    lastUpdated
  }
}

export default connect(mapStateToProps)(LatestFromBlog)
