import {combineReducers} from "redux";
import {
  REQUEST_SEARCH_RESULTS,
  RECEIVE_SEARCH_RESULTS,
  CHANGE_QUERY_STRING,
  CHANGE_PAGINATION,
  CHANGE_SEARCH_LABEL
} from '../actions/searchActions'

const initialState = {
  queryString: '',
  isFetching: false,
  searchResults: [],
  pageNumber: 1,
  searchLabel: '',
  searchFacets: []
}
const search = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_SEARCH_RESULTS:
      return Object.assign({}, state, {
        queryString: action.queryString,
        isFetching: true,
        searchResults: []
      })
    case RECEIVE_SEARCH_RESULTS:
      return Object.assign({}, state, {
        isFetching: false,
        searchResults: action.results || [],
        searchInformation: action.searchInformation,
        searchFacets: action.facets || state.searchFacets
      })
    case CHANGE_QUERY_STRING:
      return Object.assign({}, state, {
        queryString: action.queryString
      })
    case CHANGE_PAGINATION:
      return Object.assign({}, state, {
        pageNumber: action.pageNumber
      })
    case CHANGE_SEARCH_LABEL:
      return Object.assign({}, state, {
        searchLabel: (action.searchLabel === 'all') ? '' : action.searchLabel
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({
  search
})

export default rootReducer